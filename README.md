# ANTsTemplatesMirror

Personal Mirror for the ANTs Templates

Source: [https://figshare.com/articles/ANTs_ANTsR_Brain_Templates/915436](https://figshare.com/articles/ANTs_ANTsR_Brain_Templates/915436)

If you use these, please cite:

Avants, Brian; Tustison, Nick (2018): ANTs/ANTsR Brain Templates. figshare. Dataset. https://doi.org/10.6084/m9.figshare.915436.v2


Source: [https://figshare.com/articles/ANTs_files_for_mni_icbm152_nlin_sym_09a/8061914](https://figshare.com/articles/ANTs_files_for_mni_icbm152_nlin_sym_09a/8061914)

If you use the MNI template, please cite:

Tustison, Nick; Avants, Brian (2019): ANTs_files_for_mni_icbm152_nlin_sym_09a. figshare. Dataset. https://doi.org/10.6084/m9.figshare.8061914.v1